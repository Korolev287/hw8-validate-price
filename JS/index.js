// Обработчик события нужнен для того, чтобы мы могли реагировать на события 
// (щелчок мыши, нажатие клавиши на клавиатуре, изменение размера области просмотра, 
// завершение загрузки документа и т.д.,) 
// или другими словами выполнять определённые действия, когда они произойдут.


const validElement = document.createElement("span");
const priceNode = document.getElementById('priceNode')
const priceInput = document.getElementById('input')

validElement.className = "spanValid";
priceNode.before(validElement);

priceInput.addEventListener('blur', checkInputValue)
priceInput.addEventListener('focus', focusInput)

function checkInputValue() {
  let carentInputValue = priceInput.value;
  console.log(typeof carentInputValue);
  if (+carentInputValue > 0){
    validElement.innerHTML = `Текущая цена: ${carentInputValue} `;
    validElement.after(createRemoveButton())
    priceInput.classList.add('textValid');
  } else{
    priceNode.after(createElementInValid())
  }
};

function createRemoveButton() {
  const itemButton = document.createElement("button");
  itemButton.innerHTML = "x";
  itemButton.id = "button";
  itemButton.addEventListener("click", function () {
    document.querySelector(".spanValid").remove();
    document.querySelector("button").remove();
    priceInput.value = "";
  });
  return(itemButton);
};

function createElementInValid() {
  let newElement = document.createElement("span");
  newElement.innerHTML = `Please enter correct price`;
  newElement.id = "spanInValid";
  priceInput.classList.add('textInValid');
  return newElement;
}

function focusInput() {
  const spanInValid = document.getElementById("spanInValid");
  if (spanInValid){
    spanInValid.remove();
    priceInput.classList.remove('textInValid');
  }
  validElement.innerHTML = "";
  const button = document.getElementById("button");
  if (button){
    button.remove();
    priceInput.classList.remove('textValid');
  }
}
